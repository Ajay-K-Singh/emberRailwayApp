import ApplicationAdapter from './application';
import config from '../config/environment';

export default ApplicationAdapter.extend({
    queryRecord(store, type, query) {
        let url = this.buildURL(type.modelName, null, null, 'queryRecord', query);
        const pnr = query['pnrNumber'];
        const pnrText = 'pnr';
        const apiKey = config.apiKey;
        const apikeytext = 'apikey';
        const URL = `${url}/${pnrText}/${pnr}/${apikeytext}/${apiKey}/`;
        return this.ajax(URL, 'GET', {});
    }
});