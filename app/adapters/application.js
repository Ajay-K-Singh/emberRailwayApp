import DS from 'ember-data';
import config from '../config/environment';

export default DS.RESTAdapter.extend({
    host: config.apiHost,
    namespace: config.apiNamespace,
    headers: {
        contentType: 'application/json; charset=utf-8'
    }
});
