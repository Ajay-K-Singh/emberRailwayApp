import ApplicationAdapter from './application';
import config from '../config/environment';

export default ApplicationAdapter.extend({
    queryRecord(store, type, query) {
        let url = this.buildURL(type.modelName, null, null, 'queryRecord', query);
        const trainNumber = query.trainNumber;
        const trainText = 'train';
        const dateText = 'date';
        const date = query.queryDate;
        const apiKey = config.apiKey;
        const apikeytext = 'apikey';
        const URL = `${url}/${trainText}/${trainNumber}/${dateText}/${date}/${apikeytext}/${apiKey}/`;
        return this.ajax(URL, 'GET', {});
    }
});