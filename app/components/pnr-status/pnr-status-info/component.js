import Component from '@ember/component';
import pnrStatus from '../../../models/pnr-status';
import {set } from '@ember/object';
import { inject } from '@ember/service';


export default Component.extend({
    store: inject(),
    responseAwaited: null,
    pnrStatus: null,

    init(...args) {
        this._super(...args);
        this.setPnrStatusInfo(this.get('model'));
    },

    setPnrStatusInfo(pnr) {
        const pnrNumber = pnr.pnr_number;
        set(this, 'responseAwaited', true);
        this.store.queryRecord('pnr-status', { pnrNumber })
            .then((response) => {
                set(this, 'responseAwaited', false);
                set(this, 'pnrStatus', response);
            });
    }
});