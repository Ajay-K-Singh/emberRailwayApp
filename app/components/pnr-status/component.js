import Component from '@ember/component';

export default Component.extend({
    didRender() {
        $('#pnr-number').focus();
    },
    actions: {
        onCheckingStatus(pnr) {
            this.sendAction('onCheckingStatus', pnr);
        }
    }
});