import Component from '@ember/component';
import {set } from '@ember/object';
import { inject } from '@ember/service';

export default Component.extend({
    store: inject(),
    responseAwaited: false,
    trainRouteInfo: null,
    trainStationInfo: null,
    trainRouteFound: false,
    trainRouteFetched: true,
    sourceStation: null,

    init(...args) {
        this._super(...args);
        set(this, 'responseAwaited', true);
        set(this, 'trainRouteFound', false)
        this.getRouteInfo(this.get('model'));
    },

    getRouteInfo(model) {
        const trainNumber = model.train_number;
        this.store.queryRecord('route', { trainNumber })
            .then((response) => {
                set(this, 'trainRouteInfo', response);
                this.setTrainStationInfo();
                set(this, 'responseAwaited', false);
                set(this, 'trainRouteFound', true)
                set(this, 'responseAwaited', false);
                set(this, 'trainRouteFound', true);
                set(this, 'trainRouteFetched', false);
            });
    },

    setTrainStationInfo() {
        const trainRouteInfo = this.get('trainRouteInfo.route');
        const trainInfo = [];
        trainRouteInfo.forEach(station => {
            const stationInfo = {};
            stationInfo['number'] = station.no;
            if (station.scharr === "SOURCE") {
                set(this, 'sourceStation', station.get('station.name'))
            }
            stationInfo['scheduledArrival'] = station.scharr;
            stationInfo['scheduledDeparture'] = station.schdep;
            stationInfo['distance'] = station.distance;
            stationInfo['coords'] = {
                lat: station.get('station.lat'),
                lng: station.get('station.lng')
            }
            stationInfo['name'] = {
                stationName: station.get('station.name'),
                stationCode: station.get('station.code')
            }
            stationInfo['haltsFor'] = station.halt;
            stationInfo['travelDay'] = station.day;
            trainInfo.push(stationInfo);
        });
        set(this, 'trainStationInfo', trainInfo);
    }
});