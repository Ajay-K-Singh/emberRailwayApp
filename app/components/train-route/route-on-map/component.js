import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    maps: service(),

    didInsertElement() {
        this._super(...arguments);
        let mapElement = this.get('maps').getStationMarkersOnMap(this.get('trainStationInfo'));
        this.$('.map-canvas').append(mapElement);
    }
});