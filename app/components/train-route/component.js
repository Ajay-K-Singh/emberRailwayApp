import Component from '@ember/component';
import { inject } from '@ember/service';
import {set } from '@ember/object';
import $ from 'jquery';
import { later } from '@ember/runloop';

export default Component.extend({
    store: inject(),
    autoSuggestResponse: null,
    trainNameOrNumber: null,

    init() {
        this._super(...arguments);
    },

    didRender() {
        $('#autocomplete').focus();
    },

    actions: {
        onCheckingRoute(nameOrnumber) {
            let trainNumber = nameOrnumber.split(" ").pop();
            this.sendAction('trainRouteFound', trainNumber);
        },
        onClearInput() {

        },
        trainName() {
            set(this, 'trainNameOrNumber', this.get('nameOrNumber'));
            this.store.unloadAll('suggest-train');
            this.store.unloadAll('common/train');
            this.store.unloadAll('common/class');
            this.store.unloadAll('common/day');
            set(this, 'autoSuggestResponse', null);
            if (this.get('trainNameOrNumber').length > 2) {
                later(this, this.getAutoSuggestions(), 300);
            }
        }
    },
    getAutoSuggestions() {
        let trainNameOrNumber = this.get('trainNameOrNumber');
        this.store.queryRecord('suggest-train', { trainNameOrNumber })
            .then((response) => {
                set(this, 'autoSuggestResponse', response);
                this.setAutoCompleteSuggestions()
            });
    },

    setAutoCompleteSuggestions() {
        if (this.get('autoSuggestResponse')) {
            let data = {};
            this.get('autoSuggestResponse').trains.forEach(trainInfo => {
                let trainInformation = {};
                trainInformation[`${trainInfo.name} ${trainInfo.number}`] = null;
                Object.assign(data, trainInformation);
            });
            $('#autocomplete').autocomplete({
                data
            })
        }
    }
})