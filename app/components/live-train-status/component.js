import Component from '@ember/component';
import {set } from '@ember/object';
import { inject } from '@ember/service';

export default Component.extend({
    store: inject(),
    responseAwaited: null,
    trainLiveStatus: null,
    liveTrainStatusFound: false,
    trainRunningStatus: null,
    liveStatusFound: true,

    init(...args) {
        this._super(...args);
    },

    actions: {
        onLiveTrainStatus(trainNumber, date) {
            let queryDate = date.split("-").reverse().join("-");
            set(this, 'responseAwaited', true);
            this.store.queryRecord('live-train-status', { trainNumber, queryDate })
                .then((response) => {
                    set(this, 'trainLiveStatus', response);
                    this.setLiveStatusInfo();
                    set(this, 'liveStatusFound', false);
                    set(this, 'responseAwaited', false);
                    set(this, 'liveTrainStatusFound', true);
                });
        }
    },

    setLiveStatusInfo() {
        const liveStatusOfTrain = this.get('trainLiveStatus');
        let trainRunningStatus = {};
        const currentStationCoords = {
            lat: liveStatusOfTrain.get('current_station.lat'),
            lng: liveStatusOfTrain.get('current_station.lng')
        };
        trainRunningStatus['currentStation'] = {
            name: {
                stationName: liveStatusOfTrain.get('current_station.name')
            }
        };
        trainRunningStatus['currentStationCoords'] = currentStationCoords;
        trainRunningStatus['currentPosition'] = liveStatusOfTrain.position;
        const routeInfo = [];
        liveStatusOfTrain.route.forEach(staionOnRoute => {
            let routeStationAndInfo = {};
            routeStationAndInfo['actualArrival'] = staionOnRoute.actarr;
            routeStationAndInfo['actualArrivalDate'] = staionOnRoute.actarr_date;
            routeStationAndInfo['actualDeparture'] = staionOnRoute.actdep;
            routeStationAndInfo['day'] = staionOnRoute.day;
            routeStationAndInfo['distance'] = staionOnRoute.distance;
            routeStationAndInfo['hasArrived'] = staionOnRoute.has_arrived;
            routeStationAndInfo['hasDeparted'] = staionOnRoute.has_departed;
            routeStationAndInfo['lateByMins'] = staionOnRoute.latemin;
            routeStationAndInfo['scheduledArrival'] = staionOnRoute.scharr;
            routeStationAndInfo['scheduledArrivalDate'] = staionOnRoute.scharr_date;
            routeStationAndInfo['scheduledDeparture'] = staionOnRoute.schdep;
            routeStationAndInfo['distanceFromSource'] = staionOnRoute.station.get('distance');
            let stationCoords = {
                lat: staionOnRoute.station.get('lat'),
                lng: staionOnRoute.station.get('lng'),
            };
            routeStationAndInfo['name'] = {
                stationName: staionOnRoute.station.get('name'),
            }
            routeStationAndInfo['stationCoords'] = stationCoords;
            routeInfo.push(routeStationAndInfo);
        });
        trainRunningStatus['routeInfo'] = routeInfo;

        set(this, 'trainRunningStatus', trainRunningStatus);
    }
});