import DS from 'ember-data';
import { merge } from '@ember/polyfills';

export default DS.RESTSerializer.extend({
    normalizeQueryRecordResponse(store, primaryModelClass, payload, id) {
        const payloadTemp = {};
        payloadTemp[primaryModelClass.modelName] = payload;
        return this._super(store, primaryModelClass, payloadTemp, id);
    },

    keyForAttribute(attr) {
        return attr;
    },

    serializeIntoHash(hash, type, record, options) {
        merge(hash, this.serialize(record, options));
    }
});