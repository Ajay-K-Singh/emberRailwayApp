import DS from 'ember-data';
import BaseSerializer from './base-serializer';

export default BaseSerializer.extend(DS.EmbeddedRecordsMixin, {
    primaryKey: "debit",

    attrs: {
        trains: {
            embedded: 'always'
        }
    }
});