import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
  primaryKey: 'number',
  attrs: {
    days: {
      embedded: 'always'
    },
    classes: {
      embedded: 'always'
    }
  }
});
