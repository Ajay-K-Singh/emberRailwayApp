import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
    primaryKey: "scharr",
    attrs: {
        station: {
            embedded: 'always'
        }
    }
});