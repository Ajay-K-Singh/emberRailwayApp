import DS from 'ember-data';
import BaseSerializer from './base-serializer';

export default BaseSerializer.extend(DS.EmbeddedRecordsMixin, {
    primaryKey: "debit",
    attrs: {
        train: {
            embedded: 'always'
        },
        route: {
            embedded: 'always'
        },
        current_station: {
            embedded: 'always'
        }
    }
});