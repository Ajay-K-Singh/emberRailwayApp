import DS from 'ember-data';
import BaseSerializer from './base-serializer';

export default BaseSerializer.extend(DS.EmbeddedRecordsMixin,{
    primaryKey: "debit",
    attrs: {
        boarding_point: {
          embedded: 'always'
        },
        from_station: {
          embedded: 'always'
        },
        to_station: {
            embedded: 'always'
        },
        reservation_upto: {
          embedded: 'always'
        },
        journey_class: {
          embedded: 'always'
        },
        train: {
          embedded: 'always'
        },
        passengers: {
          embedded: 'always'
        }
      }
});
