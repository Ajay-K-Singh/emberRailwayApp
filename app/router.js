import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
    location: config.locationType,
    rootURL: config.rootURL
});

Router.map(function() {
    this.route('live-train-status');
    this.route('train-route', function() {})

    this.route('train', function() {
        this.route('train-route', { path: '/train-route/:train_number' });
        this.route('index');
    });
    this.route('pnr', function() {
        this.route('pnr-status', { path: '/pnr-status/:pnr_number' });
        this.route('index');
    });
});

export default Router;