import DS from 'ember-data';

export default DS.Model.extend({
    response_code: DS.attr('number'),
    debit: DS.attr('number'),
    trains: DS.hasMany('common/train')
});