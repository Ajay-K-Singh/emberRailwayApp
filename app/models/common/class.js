import DS from 'ember-data';

export default DS.Model.extend({
    available: DS.attr('string'),
    code: DS.attr('string'),
    name: DS.attr('string')
});