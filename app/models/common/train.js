import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    number: DS.attr('string'),
    days: DS.hasMany('common/day'),
    classes: DS.hasMany('common/class')
});
