import DS from 'ember-data';

export default DS.Model.extend({
    no: DS.attr('number'),
    scharr: DS.attr('string'),
    schdep: DS.attr('string'),
    distance: DS.attr('number'),
    halt: DS.attr('number'),
    day: DS.attr('number'),
    station: DS.belongsTo('common/name-code')
});