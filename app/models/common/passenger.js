import DS from 'ember-data';

export default DS.Model.extend({
    no: DS.attr('number'),
    current_status: DS.attr('string'),
    booking_status: DS.attr('string')
});