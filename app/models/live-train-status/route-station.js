import DS from 'ember-data';

export default DS.Model.extend({
    day: DS.attr('number'),
    station: DS.belongsTo('common/name-code'),
    has_arrived: DS.attr('boolean'),
    has_departed: DS.attr('boolean'),
    distance: DS.attr('number'),
    scharr: DS.attr('string'),
    schdep: DS.attr('string'),
    actarr: DS.attr('string'),
    actdep: DS.attr('string'),
    scharr_date: DS.attr('date'),
    actarr_date: DS.attr('date'),
    latemin: DS.attr('number'),
    status: DS.attr('string')
});