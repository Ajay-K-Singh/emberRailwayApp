import DS from 'ember-data';

export default DS.Model.extend({
    pnr: DS.attr('number'),
    doj: DS.attr('string'),
    debit: DS.attr('number'),
    response_code: DS.attr('number'),
    total_passengers: DS.attr('number'),
    chart_prepared: DS.attr('boolean'),
    from_station: DS.belongsTo('common/name-code'),
    to_station: DS.belongsTo('common/name-code'),
    boarding_point: DS.belongsTo('common/name-code'),
    reservation_upto: DS.belongsTo('common/name-code'),
    train: DS.belongsTo('common/train'),
    journey_class: DS.belongsTo('common/journey-class'),
    passengers: DS.hasMany('common/passenger')
})