import Inflector from 'ember-inflector';

const inflector = Inflector.inflector;

inflector.irregular('pnrStatus', 'pnr-status');
inflector.uncountable('route');
inflector.irregular('liveTrainStatus', 'live');
inflector.irregular('suggestTrain', 'suggest-train');

export default {};