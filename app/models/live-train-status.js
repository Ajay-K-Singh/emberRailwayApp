import DS from 'ember-data';

export default DS.Model.extend({
    response_code: DS.attr('number'),
    current_station: DS.belongsTo('common/name-code'),
    debit: DS.attr('number'),
    position: DS.attr('string'),
    train: DS.belongsTo('common/train'),
    start_date: DS.attr('date'),
    route: DS.hasMany('live-train-status/route-station')
});