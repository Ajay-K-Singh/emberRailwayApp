import DS from 'ember-data';

export default DS.Model.extend({
    debit: DS.attr('number'),
    response_code: DS.attr('number'),
    train: DS.belongsTo('common/train'),
    route: DS.hasMany('common/station')
});
