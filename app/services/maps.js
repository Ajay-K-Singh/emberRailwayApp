import Service from '@ember/service';
import MapUtil from '../utils/google-maps';

export default Service.extend({

    init() {
        this._super(...arguments)
        if (!this.get('mapUtil')) {
            this.set('mapUtil', MapUtil.create());
        }
    },

    getStationMarkersOnMap(stationInfo) {
        const element = this.createMapElement();
        this.get('mapUtil').createStationMarkersOnMap(element, stationInfo);
        return element;
    },

    getRunningStatusOnMap(liveRunningInfo) {
        const element = this.createMapElement();
        this.get('mapUtil').createLiveStatusOnMap(element, liveRunningInfo);
        return element;
    },

    createMapElement() {
        const element = document.createElement('div');
        element.className = 'map';
        return element;
    }
});