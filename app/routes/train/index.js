import Route from '@ember/routing/route';

export default Route.extend({
    actions: {
        trainRouteFound(nameOrnumber) {
            this.transitionTo('train.train-route', nameOrnumber);
        }
    }
});