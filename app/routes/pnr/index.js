import Route from '@ember/routing/route';

export default Route.extend({
    actions: {
        onCheckingStatus(pnr) {
            this.transitionTo('pnr.pnr-status', pnr);
        }
    }
});