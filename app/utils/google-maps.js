import EmberObject from '@ember/object';
import { A } from '@ember/array';

const google = window.google;

export default EmberObject.extend({
    marker: null,
    identifier: null,
    map: null,
    zoom: 6,
    defaultLat: 20.5937,
    defaultLng: 78.9629,
    stationCoordinates: A(),

    init() {
        this.set('geocoder', new window.google.maps.Geocoder());
    },

    createStationMarkersOnMap(element, stationInfo) {
        let map = this.getMap(element);
        let bounds = new google.maps.LatLngBounds();

        stationInfo.forEach(station => {
            this.getStationCoordinates(station, map, bounds);
        });
        return map;
    },

    setMarkersForStations(stationPositionCoords, station, map, bounds) {
        let infoWindow = new google.maps.InfoWindow();
        let marker = null;
        marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: stationPositionCoords
        });
        bounds.extend(stationPositionCoords);
        const markerElement = this.getMarkerElement();
        markerElement.childNodes[0].textContent = station.name.stationName;
        markerElement.childNodes[1].textContent = station.scheduledArrival;
        markerElement.childNodes[2].textContent = station.scheduledDeparture;
        markerElement.childNodes[3].textContent = station.distance;
        google.maps.event.addListener(marker, 'mouseover', (function(marker, content, infoWindow) {
            return function() {
                infoWindow.setContent(markerElement);
                infoWindow.open(map, marker);
            };
        })(marker, markerElement, infoWindow));
        google.maps.event.addListener(marker, 'mouseout', (function() {
            return function() {
                infoWindow.close();
            };
        })());
    },

    getStationCoordinates(station, map, bounds, greenColored) {
        const stationLocationName = `${station.name.stationName} Railway Station`;
        let stationPositionCoords = {};
        this.get('geocoder').geocode({
            address: stationLocationName
        }, (result, status) => {
            if (status === google.maps.GeocoderStatus.OK) {
                const location = result[0].geometry.location;
                stationPositionCoords['lat'] = location.lat();
                stationPositionCoords['lng'] = location.lng();
                this.setStationCoordinates(stationPositionCoords);
                this.setMarkersForStations(stationPositionCoords, station, map, bounds);
                this.createRoutePolyLine(this.get('stationCoordinates'), map, greenColored);
                map.fitBounds(bounds);
            }
        });
    },

    getMarkerElement() {
        const contentDiv = document.createElement('div');
        contentDiv.className = 'stationInfo';
        const stationName = document.createElement('p');
        stationName.className = 'stationName';
        const scheduledArrival = document.createElement('p');
        scheduledArrival.className = 'scheduledArrival';
        const scheduledDeparture = document.createElement('p');
        scheduledDeparture.className = 'scheduledDeparture';
        const distanceFromSource = document.createElement('p');
        distanceFromSource.className = 'distanceFromSource';
        contentDiv.appendChild(stationName);
        contentDiv.appendChild(scheduledArrival);
        contentDiv.appendChild(scheduledDeparture);
        contentDiv.appendChild(distanceFromSource);
        return contentDiv;
    },

    setStationCoordinates(stationCoords) {
        this.get('stationCoordinates').push(stationCoords);
    },

    createLiveStatusOnMap(element, liveRunningInfo) {
        let map = this.getMap(element);
        let bounds = new google.maps.LatLngBounds();
        let counter = 0;
        liveRunningInfo.routeInfo.forEach(stationInfo => {
            if (stationInfo.hasDeparted) {
                this.getStationCoordinates(stationInfo, map, bounds);
            }
            if (!stationInfo.hasArrived && counter < 1) {
                const greenColored = {
                    strokeColor: '#00FF00'
                };
                this.getStationCoordinates(liveRunningInfo.currentStation, map, bounds, greenColored);
                counter++;
            }
            if (!stationInfo.hasDeparted) {
                this.getStationCoordinates(stationInfo, map, bounds);
            }
        });
        return map;
    },

    createRoutePolyLine(coordinates, map, greenColored) {
        let counter = 0;
        let strokeColor;
        if (greenColored) {
            counter++;
        }
        if (counter < 1) {
            strokeColor = '#00FF00';
        } else {
            strokeColor = '#ff0000';
        }
        var trainRoute = new google.maps.Polyline({
            path: coordinates,
            geodesic: true,
            strokeColor: strokeColor,
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        trainRoute.setMap(map);
    },

    getMap(element) {
        const map = new google.maps.Map(element, {
            center: { lat: this.get('defaultLat'), lng: this.get('defaultLng') },
            zoom: this.get('zoom'),
            styles: this.getMapStyles()
        });
        return map;
    },

    getMapStyles() {
        return [
            { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{ color: '#263c3f' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#6b9a76' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{ color: '#38414e' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#212a37' }]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#9ca5b3' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{ color: '#746855' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#1f2835' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#f3d19c' }]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{ color: '#2f3948' }]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{ color: '#17263c' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#515c6d' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{ color: '#17263c' }]
            }
        ]
    }

});