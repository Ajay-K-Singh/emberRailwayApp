import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | live-train-status', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:live-train-status');
    assert.ok(route);
  });
});
