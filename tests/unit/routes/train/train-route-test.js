import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | train/train-route', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:train/train-route');
    assert.ok(route);
  });
});
