import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | pnr/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:pnr/index');
    assert.ok(route);
  });
});
