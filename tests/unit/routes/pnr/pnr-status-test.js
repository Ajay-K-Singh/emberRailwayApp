import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | pnr/pnr-status', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:pnr/pnr-status');
    assert.ok(route);
  });
});
